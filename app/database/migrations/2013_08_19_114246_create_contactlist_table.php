<?php

use Illuminate\Database\Migrations\Migration;

class CreateContactlistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

Schema::create('contactlist', function($table) {
			$table->increments('id');
			$table->string('name', 128);
			$table->string('position', 128);
			$table->string('mail', 128);
			$table->string('web', 128);
			$table->string('phone', 128);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

		Schema::drop('contactlist');

	}

}

