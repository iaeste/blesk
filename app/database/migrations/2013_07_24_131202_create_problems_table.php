<?php

use Illuminate\Database\Migrations\Migration;

class CreateProblemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('problems', function($table) {
			$table->increments('id');
			$table->integer('solver_id');
			$table->integer('service_id');
			$table->integer('state_id');
			$table->integer('author_id');
			$table->integer('problem_type_id');
			$table->string('title');
			$table->text('content');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('problems');
	}

}
