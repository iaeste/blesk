<?php

use Illuminate\Database\Migrations\Migration;

class CreateProblemTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('problem_types', function($table) {
			$table->increments('id');
			$table->string('problem_type_name', 128);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('problem_types');
	}

}
