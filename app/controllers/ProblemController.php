<?php

class ProblemController extends BaseController {

	public function listTable()
	{
		$problems = Problem::all();
		return View::make('pages.list')->with('problems', $problems);
	}

	public function getDatatable()
	{
		$problems = Problem::join('services','problems.service_id','=','services.id')
		->join('states','problems.state_id','=','states.id')
		->join('users','problems.solver_id','=','users.id')
		->join('problem_types','problems.problem_type_id','=','problem_types.id')
		->select(array('problems.id','problems.title','services.service_name','problem_types.problem_type_name','problems.created_at','users.username','states.state_name'));
		return Datatables::of($problems)->add_column('operations','<div class="btn-group">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
			Action
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
			<li> <a href="/index.php/show/{{$id}}"> Detail </a> </li>

			@if ($state_name=="todo")
			<li> <a href="/index.php/accept/{{$id}}">Accept</a> </li>
			@endif

			@if ($state_name=="in progress")
			<li> <a href="/index.php/close/{{$id}}">Close</a> </li>
			@endif

			<li> <a href="/index.php/edit/{{$id}}"> Edit description </a> 	</li>
			<li> <a href="/index.php/delete/{{$id}}" onclick="return confirm(\'Are you sure?\')"  >Delete</a> 	</li>
			</ul>
			</div>
			')->make();
	}


	public function getLog($problem_id)
	{

		$posts = Post::where('problem_id', '=', $problem_id)
		->join('users','posts.user_id','=','users.id')
		->select(array('posts.id', 'posts.body',  'users.username', 'posts.created_at'));
		return Datatables::of($posts)->add_column('operations', '


			<div class="btn-group">

			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Action <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
			<li> <a href="/index.php/answer/edit/{{$id}}"> Edit </a> </li>
			<li> <a href="/index.php/answer/delete/{{$id}}" onclick="return confirm(\'Are you sure?\');">Delete</a> </li>
			</ul>
			</div>

			')->remove_column('id')->make();

	}



// ################################################################ 
// ################################################################
// ######################## PROBLEM ###############################
// ################################################################ 
// ################################################################

	public function newProblemPost()
	{

		$new_problem = array(
			'author' 			=> Input::get('author_id'),
			'title' 			=> Input::get('title'),
			'problem_type'	=> Input::get('problem_type'),
			'service' 		=> Input::get('service'),
			'content' 		=> Input::get('content'),
			);

		$rules = array(
			'title' 	=> 'required|min:3|max:128',
			'content' 	=> 'required'
			);
		
		$v = Validator::make($new_problem, $rules);
		if ( $v->fails() )	{
			return Redirect::to('new')->withErrors($v)->withInput();
		}
		
		$problem = new Problem();
		
		$problem->title=$new_problem['title'];
		$problem->author_id=$new_problem['author'];
		$problem->problem_type_id=$new_problem['problem_type'];
		$problem->service_id=$new_problem['service'];
		$problem->content=$new_problem['content'];
		$problem->solver_id=1;
		$problem->state_id=1;
		$problem->save();

		return Redirect::to('/');
	}

	public function newProblemGet()
	{
		$problem_types = ProblemType::lists('problem_type_name', 'id');
		$services = Service::lists('service_name', 'id');
		return View::make('pages.new')->with('problem_types',$problem_types)->with('services',$services);
	}


	public function acceptProblem($id)
	{
		$problem = Problem::find($id);
		$problem->solver_id=1;
		if ($problem->state_id != 1){
			$errorMess="test";
			return Redirect::to('/')->with('errorMess',$errorMess); 		
		}
		$problem->state_id = 2;
		$problem->save();	

		$acceptedLog = new Post();
		$acceptedLog->problem_id = $id;
		$acceptedLog->user_id = 2;
		$acceptedLog->body = "Accepted";
		$acceptedLog->save();

		return Redirect::to('/');
	}
	public function closeProblem($id)
	{
		$problem = Problem::find($id);
		if ($problem->state_id != 2){
			$errorMess="test";
			return Redirect::to('/')->with('errorMess',$errorMess); 		
		}

		$problem->state_id = 3;
		$problem->save();

		$closedLog = new Post();
		$closedLog->problem_id = $id;
		$closedLog->user_id = 2;
		$closedLog->body = "Closed";
		$closedLog->save();

		return Redirect::to('/');
	}
	public function deleteProblem($id)
	{
		Problem::destroy($id);
		return Redirect::to('/');
	}


	public function editProblem($id)
	{
		$problem_types = ProblemType::lists('problem_type_name', 'id');
		$services = Service::lists('service_name', 'id');
		$states = State::lists('state_name', 'id');		
		$users = User::lists('username', 'id');
		$problem = Problem::find($id);



		// Setting up selection value in view  from state
		$problemState = $problem->state;
		$problemStateValue = 0;

		if ( $problemState == "done" )	{		
			$problemStateValue = 3;
		}	else if($problemState == "in progress")	{
			$problemStateValue = 2;
		}	else	{
			$problemStateValue = 1;
		}

		$problemEdited = new Post();
		$problemEdited->problem_id = $id;
		$problemEdited->user_id = 2;
		$problemEdited->body = "Edited";

		$problemEdited->save();


		return View::make('pages.edit')->with('problem_types',$problem_types)->with('services',$services)->with('states',$states)->with('users',$users)->with('problem',$problem)->with('problemStateValue',$problemStateValue);
	}

	public function updateProblem($id)
	{

		$new_problem = array(
			'title' 		=> Input::get('title'),
			'problem_type' 	=> Input::get('problem_type'),
			'service' 		=> Input::get('service'),
			'content' 		=> Input::get('content'),
			'author' 		=> Input::get('author'),
			'state'			=> Input::get('state'),
			'solver' 		=> Input::get('solver'),
			);


		$rules = array(
			'title' 	=> 'required|min:3|max:128',
			'content' 	=> 'required'
			);
		$v = Validator::make($new_problem, $rules);
		if ( $v->fails() )	{
			return Redirect::to('edit/'.$id)->withErrors($v)->withInput();
		}
		
		$problem = Problem::find($id);
		
		$problem->title=$new_problem['title'];
		$problem->author_id=$new_problem['author'];
		$problem->problem_type_id=$new_problem['problem_type'];
		$problem->service_id=$new_problem['service'];
		$problem->state_id=$new_problem['state'];
		$problem->solver_id=$new_problem['solver'];
		$problem->content=$new_problem['content'];
		$problem->save();

		return Redirect::to('/');
	}

	public function showProblem($id)
	{
		$problem = Problem::find($id);
		$users = User::lists('username', 'id');
		return View::make('pages.show')->with('problem',$problem)->with('users',$users);

	}

// ################################################################ 
// ################################################################
// ########################### ANSWER #############################
// ################################################################ 
// ################################################################



	public function newAnswerPost($id)
	{

		$new_answer = array(
			'user' 		=> Input::get('user'),
			'body' 		=> Input::get('body')

			);

			// let's setup some rules for our new data
			// I'm sure you can come up with better ones
		$rules = array(
			'body' 	=> 'required | min: 3',
			);
			// make the validator
		$v = Validator::make($new_answer, $rules);
		if ( $v->fails() )	{


			return Redirect::to('/show/'.$id)->withErrors($v)->withInput();
		}
		

		// create the new post
		
		$answer = new Post();
		$answer->body=$new_answer['body'];
		$answer->user_id=$new_answer['user'];
		//$answer->user_id=2;
		$answer->problem_id=$id;
		$answer->save();

		return Redirect::to('/show/'.$id);
	}


	public function deleteAnswer($id)
	{
		$problem_id = Post::find($id)->problem_id;
		Post::destroy($id);
		return Redirect::to('/show/'.$problem_id);
	}

	public function editAnswer($id)
	{
		$post = Post::find($id);

		return View::make('pages.editAnswer')->with('post',$post);
	}

	public function updateAnswer($id)
	{

		$new_answer = array(
			'body' 		=> Input::get('body')

			);

		$rules = array(
			'body' 	=> 'required| min: 3'
			);

		$problem_id = Post::find($id)->problem_id;
		$v = Validator::make($new_answer, $rules);
		if ( $v->fails() )	{
			return Redirect::to('answer/edit/'.$problem_id)->withErrors($v)->withInput();
		}

		$answer = Post::find($id);
		$answer->id=$id;
		$answer->body=$new_answer['body'];
		$answer->save();

		return Redirect::to('show/'.$problem_id);
	}


// ################################################################ 
// ################################################################
// ########################### CREATE #############################
// ################################################################ 
// ################################################################

	public function newStatePost()
	{

		$new_state = array(
			'state_name' 			=> Input::get('state_name'),
			);

		$rules = array(
			'state_name' 	=> 'required|min:3|max:128',

			);

		$v = Validator::make($new_state, $rules);
		if ( $v->fails() )	{
			return Redirect::to('newState')->withErrors($v)->withInput();
		}

		$state = new State();
		$state->state_name=$new_state['state_name'];
		$state->save();

		return Redirect::to('/');
	}

	public function newStateGet()
	{
		return View::make('pages.newState');
	}

	public function newServicePost()
	{

		$new_service = array(
			'service_name' 			=> Input::get('service_name'),
			);

		$rules = array(
			'service_name' 	=> 'required|min:3|max:128',
			);

		$v = Validator::make($new_service, $rules);
		if ( $v->fails() )	{
			return Redirect::to('newService')->withErrors($v)->withInput();
		}

		$service = new Service();
		$service->service_name=$new_service['service_name'];
		$service->save();

		return Redirect::to('/');
	}

	public function newServiceGet()
	{
		return View::make('pages.newService');
	}

	public function newProblemTypePost()
	{

		$new_problem_type = array(
			'problem_type_name' 			=> Input::get('problem_type_name'),
			);

		$rules = array(
			'problem_type_name' 	=> 'required|min:3|max:128',

			);

		$v = Validator::make($new_problem_type, $rules);
		if ( $v->fails() )	{
			return Redirect::to('newProblemType')->withErrors($v)->withInput();
		}

		$problem_type = new ProblemType();
		$problem_type->problem_type_name=$new_problem_type['problem_type_name'];
		$problem_type->save();

		return Redirect::to('/');
	}

	public function newProblemTypeGet()
	{
		return View::make('pages.newProblemType');
	}


	public function logoutPost()
	{
		return Redirect::to('/');
	}

	public function logoutGet()
	{
		return View::make('pages.logout');
	}


// ################################################################ 
// ################################################################
// ########################### MERGE ##############################
// ################################################################ 
// ################################################################

	public function getMergetable()
	{
		$problems = Problem::join('services','problems.service_id','=','services.id')
		->join('states','problems.state_id','=','states.id')
		->join('users','problems.solver_id','=','users.id')
		->join('problem_types','problems.problem_type_id','=','problem_types.id')
		->select(array('problems.id','problems.title','services.service_name','problem_types.problem_type_name','problems.created_at','users.username','states.state_name'));
		return Datatables::of($problems)->add_column('chk','<div class="btn-group">
			<input type="checkbox" name = "checkbox[]" value={{$id}}> 
			</div>
			')->make();

	}

	public function newMergeProblemPost(){


		$ids=Input::get('checkbox');
		$title= array();
		$content = "";
		
		if(!$ids){
			return Redirect::to('/');	
		}	
		foreach ($ids as $id) {

			$problem_types = ProblemType::lists('problem_type_name', 'id');
			$services = Service::lists('service_name', 'id');
			$states = State::lists('state_name', 'id');		
			$users = User::lists('username', 'id');
			
			$prob = Problem::find($id);
			//$tit=;
			$title[$prob->title] = $prob->title;
			$content .= "\n".$prob['content'];		
			Problem::destroy($id);


		}
		return View::make('pages.newMergeProblem')->with('title',$title)->with('problem_types',$problem_types)->with('services',$services)->with('content',$content)->with('states',$states)->with('users',$users);

	}    


	public function newMergeProblemGet()
	{

		return View::make('pages.newMergeProblem')->with('problem_types',$problem_types)->with('services',$services);
		
	}


	public function listMergeTable()
	{
		$problems = Problem::all();
		return View::make('pages.mergeList')->with('problems', $problems);

	}

	public function saveMergePost()
	{


		$new_problem = array(
			'author' 			=> Input::get('author_id'),
			'title' 			=> Input::get('title'),
		//	'title' 			=> "new title",
			'problem_type'	=> Input::get('problem_type'),
			'service' 		=> Input::get('service'),
			'content' 		=> Input::get('content'),
			);

			// let's setup some rules for our new data
			// I'm sure you can come up with better ones
		$rules = array(
			//'title' 	=> 'required|min:3|max:128',
			'content' 	=> 'required'
			);
			// make the validator
		$v = Validator::make($new_problem, $rules);
		if ( $v->fails() )	{

			return Redirect::to('/')->withErrors($v)->withInput();
		}
		

		$problem = new Problem();
		
		$problem->title=$new_problem['title'];
		$problem->author_id=$new_problem['author'];
		$problem->problem_type_id=$new_problem['problem_type'];
		$problem->service_id=$new_problem['service'];
		$problem->content=$new_problem['content'];
		$problem->solver_id=1;
		$problem->state_id=1;
		$problem->save();
		//return Redirect::to('show/'+$id);
		return Redirect::to('/');


	}
	public function saveMergeGet(){

		$problem_types = ProblemType::lists('problem_type_name', 'id');
		$services = Service::lists('service_name', 'id');
		return View::make('pages.new')->with('problem_types',$problem_types)->with('services',$services);

	}


// ################################################################ 
// ################################################################
// ########################## CONTACT #############################
// ################################################################ 
// ################################################################


	public function listContactList()
	{
		$contactlist = Contact::all();
		return View::make('pages.list')->with('contactlist', $contactlist);
	}



	public function newContactPost()
	{

		$new_contact = array(
			'name' 		=> Input::get('name'),
			'position' 	=> Input::get('position'),
			'web' 		=> Input::get('web'),
			'phone' 		=> Input::get('phone'),
			'mail' 		=> Input::get('mail'),
			);


		$rules = array(
			'name' 	=> 'required|min:3|max:128',
			// 'position' 	=> 'required|min:3|max:128',
			// 'web' 	=> 'required|min:3|max:128',
			// 'phone' 	=> 'required|min:3|max:128',
			// 'mail' 	=> 'required|min:3|max:128',

			);
		$v = Validator::make($new_contact, $rules);
		if ( $v->fails() )	{
			return Redirect::to('newContact')->withErrors($v)->withInput();
		}

		$contact = new Contact();
		$contact->name=$new_contact['name'];
		$contact->position=$new_contact['position'];
		$contact->web=$new_contact['web'];
		$contact->phone=$new_contact['phone'];
		$contact->mail=$new_contact['mail'];
		$contact->save();
		
		return Redirect::to('/');
	}

	public function newContactGet()
	{
		return View::make('pages.newContact');
	}


	public function editContact($id){
	
		$contact = Contact::find($id);
		return View::make('pages.editContact')->with('contact',$contact);
	}

	public function updateContact($id)
	{

		$new_contact = array(
			'name' 		=> Input::get('name'),
			'position' 	=> Input::get('position'),
			'web' 		=> Input::get('web'),
			'phone' 		=> Input::get('phone'),
			'mail' 		=> Input::get('mail'),
			);


		$rules = array(
			'name' 	=> 'required|min:3|max:128',
			// 'position' 	=> 'required|min:3|max:128',
			// 'web' 	=> 'required|min:3|max:128',
			// 'phone' 	=> 'required|min:3|max:128',
			// 'mail' 	=> 'required|min:3|max:128',

			);
		$v = Validator::make($new_contact, $rules);
		if ( $v->fails() )	{
			return Redirect::to('editContact/'.$id)->withErrors($v)->withInput();

		}
		
		$contact = Contact::find($id);
		
		$contact->name=$new_contact['name'];
		$contact->position=$new_contact['position'];
		$contact->web=$new_contact['web'];
		$contact->phone=$new_contact['phone'];
		$contact->mail=$new_contact['mail'];
		$contact->save();

		return Redirect::to('/');
	}

	public function deleteContact($id)
	{
		Contact::destroy($id);
		return Redirect::to('/');
	}

}

?>
