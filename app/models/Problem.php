<?php

class Problem extends Eloquent {

	 protected $fillable = array('*');
	 
	 public function solver()
	 {
	 	return $this->belongsTo('User','solver_id');
	 }

	 public function service()
	 {
	 	return $this->belongsTo('Service','service_id');
	 }

	 public function state()
	 {
	 	return $this->belongsTo('State','state_id');
	 }

	 public function author()
	 {
	 	return $this->belongsTo('User','author_id');
	 }

	 public function problemType()
	 {
	 	return $this->belongsTo('ProblemType','problem_type_id');
	 }

	 public function contactList()
	 {
	 	return $this->belongsTo('contactList','contact_id');
	 }
}

?>
