@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'newState', 'method' => 'POST')) }}
 <!-- title field -->
 
 <p>{{ Form::label('state_name', 'State name') }}</p>
 {{ $errors->first('state_name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('state_name', Input::old('state_name')) }}</p>

 <!-- submit button -->

<button type="submit" class="btn btn-primary"> Create </button>
<a href="/" class = "btn btn-default" > Back </a>

 {{ Form::close() }}
@stop
