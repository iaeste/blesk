@extends('templates.layout')
@section('content')
 @foreach ($posts as $post)
 <div class="post">
 <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
 	<h1>Blesk</h1>
 <h1>{{ HTML::link('view/'.$post->id, $post->title) }}</h1>
 <p>{{ substr($post->body,0, 120).' [..]' }}</p>
 <p>{{ HTML::link('view/'.$post->id, 'Read more &rarr;') }}</p>
 </div>
 @endforeach
@stop
