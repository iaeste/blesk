@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'newContact', 'method' => 'POST')) }}
 <!-- title field -->
 
 <p>{{ Form::label('name', 'Name') }}</p>
 {{ $errors->first('name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('name', Input::old('name')) }}</p>


 <p>{{ Form::label('position', 'Position') }}</p>
 {{ $errors->first('position', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('position', Input::old('position')) }}</p>

 <p>{{ Form::label('web', 'Web') }}</p>
 {{ $errors->first('web', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('web', Input::old('web')) }}</p>

 <p>{{ Form::label('phone', 'Phone') }}</p>
 {{ $errors->first('phone', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('phone', Input::old('phone')) }}</p>

 <p>{{ Form::label('mail', 'Email') }}</p>
 {{ $errors->first('mail', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('mail', Input::old('mail')) }}</p>


<button type="submit" class="btn btn-primary"> Create </button>
<a href="/" class = "btn btn-default" > Back </a>

 {{ Form::close() }}
@stop
