@extends('templates.layout')
@section('content')

{{ Form::open(array('url' => 'mergeSave/', 'method' => 'POST')) }} 


<script type="text/javascript">
$(document).ready(function() {
	$('#datatable').dataTable( {
		"sAjaxSource": "/index.php/merge-prob/",
		"aoColumnDefs": [
    { 'bSortable': false, 'aTargets': [ 7 ] }
    ],
    "fnCreatedRow": function( nRow, aData, iDataIndex ) {
   }

 } );

} );

</script>

{{ Form::hidden('author_id', 1) }}
<p>{{ Form::label('title', 'Subject') }}</p>
{{ $errors->first('title', '<p class="alert alert-danger">:message</p>') }}
<p>{{ Form::select('title',$title, Input::old('title')) }}</p>


<p>{{ Form::label('problem_type', 'Type of problem') }}</p>
{{ $errors->first('problem_type', '<p class="error">:message</p>') }}
<p>{{ Form::select('problem_type', $problem_types ,Input::old('problem_type')) }}</p>


<p>{{ Form::label('service', 'Service') }}</p>
{{ $errors->first('service', '<p class="error">:message</p>') }}
<p>{{ Form::select('service',  $services ,Input::old('service')) }}</p>

<p>{{ Form::label('content', 'Description') }}</p>
{{ $errors->first('content', '<p class="alert alert-danger">:message</p>') }}
<p>{{ Form::textarea('content', $content, Input::old('content')) }}</p> 


<button type="submit" class="btn btn-large btn-primary" > Merge </button>
<a href="/" class = "btn btn-large btn-primary" > Back </a>

{{ Form::close() }}

@stop


