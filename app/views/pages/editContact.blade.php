@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'updateContact/'.$contact->id, 'method' => 'POST')) }}
 <!-- title field -->
 <p>{{ Form::label('name', 'Name') }}</p>
 {{ $errors->first('name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('name', $contact->name) }}</p>

  <p>{{ Form::label('position', 'Position') }}</p>
 {{ $errors->first('position', '<p class="error">:message</p>') }}
 <p>{{ Form::text('position' ,$contact->position) }}</p>

  <p>{{ Form::label('web', 'Web') }}</p>
 {{ $errors->first('web', '<p class="error">:message</p>') }}
 <p>{{ Form::text('web',$contact->web) }}</p>

 <p>{{ Form::label('phone', 'Phone') }}</p>
 {{ $errors->first('phone', '<p class="error">:message</p>') }}
 <p>{{ Form::text('phone' ,$contact->phone) }}</p>


 <p>{{ Form::label('mail', 'Email') }}</p>
 {{ $errors->first('mail', '<p class="error">:message</p>') }}
<p>{{ Form::text('mail', $contact->mail) }}</p>

 <button type="submit" class="btn btn-large btn-primary"> Update </button>
 <a href="/" class = "btn btn-large btn-primary" > Back </a>
 {{ Form::close() }}

@stop
