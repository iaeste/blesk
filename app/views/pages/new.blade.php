@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'new', 'method' => 'POST')) }}

 {{ Form::hidden('author_id', 1) }}
 <p>{{ Form::label('title', 'Subject') }}</p>
 {{ $errors->first('title', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('title', Input::old('title')) }}</p>

  <p>{{ Form::label('problem_type', 'Type of problem') }}</p>
 {{ $errors->first('problem_type', '<p class="error">:message</p>') }}
 <p>{{ Form::select('problem_type', $problem_types ,Input::old('problem_type')) }}</p>


 <p>{{ Form::label('service', 'Service') }}</p>
 {{ $errors->first('service', '<p class="error">:message</p>') }}
 <p>{{ Form::select('service',  $services ,Input::old('service')) }}</p>

 <p>{{ Form::label('content', 'Description') }}</p>
 {{ $errors->first('content', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::textarea('content', Input::old('content')) }}</p>


<button type="submit" class="btn btn-large btn-primary" onclick="this.disabled=true;forms[0].submit();"> Create </button>
<a href="/" class = "btn btn-large btn-primary" > Back </a>

 {{ Form::close() }}
@stop
