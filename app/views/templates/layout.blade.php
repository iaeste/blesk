<!DOCTYPE HTML>

<html lang="en-GB">
<head>
 <meta charset="UTF-8">
<title href = "/">Blesk</title>
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">

{{ HTML::style('css/bootstrap.css') }}
{{ HTML::script('js/jquery-1.10.2.js') }}
{{ HTML::script('js/jquery.dataTables.js') }}
{{ HTML::script('js/bootstrap.js') }}
</head>

<script>
// $('body').on('.nav li a', 'click', function()
// {
//     var $thisLi = $(this).parents('li:first');
//     var $ul = $thisLi.parents('ul:first');
//     if (!$thisLi.hasClass('active'))
//     {
//         $ul.find('li').removeClass('active');
//         $thisLi.addClass('active');
//     }
// });


</script>

<?php 

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo 'class="active"';
}

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="/"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
} 

?>

<body>

	<div class="span12 offset1" style="float: left;">
		<div class="navbar">
		  <div class="navbar-inner">
		    <a class="brand" href="/" onclick=>Blesk</a>
		    <ul class="nav">
		      <!-- <li <?=echoActiveClassIfRequestMatches("/")?> ><a href="/">Home</a></li> -->
		      <li ><a href="/index.php/new">New Problem</a></li>

			  <li class="dropdown">
			    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			      Administration
			      <b class="caret"></b>
			    </a>
			    <ul class="dropdown-menu">
	      		    <li ><a href="/index.php/newState">New State</a></li>
					<li ><a href="/index.php/newService">New Service</a></li>
					<li ><a href="/index.php/newProblemType">New Problem Type</a></li>
					<li ><a href="/index.php/newContact">New Contact</a></li>				
					<li ><a href="/index.php/merge-problems" >Merge Problems</a></li>


			    </ul>
			  </li>
			  <li ><a href="/index.php/logout">Log out</a></li>
		    </ul>
		  </div>
		</div>
	</div>

	<div class="span12 offset1" style="clear: both;">
		<div class="row-fluid">
			@yield('content')
		</div>
	</div>
</body>
</html>

